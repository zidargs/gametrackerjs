import JSONCResource from "/emcJS/data/resource/file/JSONCResource.js";

export default await JSONCResource.get("/database/items.json");
